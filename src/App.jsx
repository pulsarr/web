import React from "react";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { Button } from "react-bulma-components";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Button color="primary">My Bulma button</Button>
      </header>
    </div>
  );
}

export default App;
