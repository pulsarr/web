import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders My Bulma button link", () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/My Bulma button/i);
  expect(linkElement).toBeInTheDocument();
});
