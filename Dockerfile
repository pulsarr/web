# Foundation for all builds / ci
FROM node:12.14 AS build

WORKDIR /app
COPY package*.json ./

RUN npm install

COPY . ./

RUN npm run build

# Prod ready runable image/container 
FROM nginx:1.12-alpine as prod
COPY --from=build  /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
